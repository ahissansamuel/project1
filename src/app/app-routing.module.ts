import { ListArticlesComponent } from './espace-info/list-articles/list-articles.component';
import { EspaceInfoComponent } from './espace-info/espace-info.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  {path: '', redirectTo: '/list-articles', pathMatch: 'full'},
  { path: 'espace-info/:id', component: EspaceInfoComponent},
  {path: 'list-articles', component: ListArticlesComponent},
  { path: '**', redirectTo: '/list-articles'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
