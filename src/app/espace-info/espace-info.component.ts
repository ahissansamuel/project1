import { Constants } from './../_helpers/Constants';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
declare const scrollWindow: any;

@Component({
  selector: 'app-espace-info',
  templateUrl: './espace-info.component.html',
  styleUrls: ['./espace-info.component.scss']
})
export class EspaceInfoComponent implements OnInit {

  selectedArticle: any;

  constructor(private route:ActivatedRoute) { }

  ngOnInit(): void {
    const id = parseInt( this.route.snapshot.paramMap.get('id'), 10 );

    console.log('Article N°', typeof(id));
    this.selectedArticle = Constants.listArticle.find(x => x.id == id);
  }

}

