import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EspaceInfoComponent } from './espace-info.component';

describe('EspaceInfoComponent', () => {
  let component: EspaceInfoComponent;
  let fixture: ComponentFixture<EspaceInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EspaceInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EspaceInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
