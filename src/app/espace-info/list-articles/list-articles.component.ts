import { Constants } from './../../_helpers/Constants';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-list-articles',
  templateUrl: './list-articles.component.html',
  styleUrls: ['./list-articles.component.scss']
})
export class ListArticlesComponent implements OnInit {

  classColor: any; //add class hover
  listArticle = Constants.listArticle;

  page = 1; // Pagination counter
  constructor() { }

  ngOnInit(): void {
  }

}
