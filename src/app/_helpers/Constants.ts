export class Constants {

  static globalSummary = `Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reiciendis, eius mollitia suscipit,
  quisquam doloremque distinctio perferendis et doloribus unde architecto optio laboriosam porro
  adipisci sapiente officiis nemo accusamus ad praesentium? Esse minima nisi et. Dolore perferendis,
  enim praesentium omnis, iste doloremque quia officia optio deserunt molestiae voluptates soluta
  architecto tempora.`;

  static listArticle = [
    {id: 1, title: 'L’Afrique comme laboratoire de test contre le Covid-19, Didier Drogba dit non : « Ne prenez pas les Africains comme des cobayes humains, c’est dégoûtant »',
    summary: Constants.globalSummary, image: '../../../assets/images/no-inafrica.jpg', datePub: '03/04/2020', readTime: '2'},
    {id: 2, title: 'Lutte contre la propagation du COVID-19 : les exportateurs nationaux de cacao offrent 25 millions FCFA au gouvernement ivoirien',
    summary: Constants.globalSummary, image: '../../../assets/images/funding.jpg', datePub: '03/04/2020', readTime: '5'},
    {id: 3, title: 'Covid-19: La Côte d’Ivoire enregistre 04 nouveaux cas et 6 nouveaux cas de guérisons',
    summary: Constants.globalSummary, image: '../../../assets/images/good-news.jpg', datePub: '03/04/2020', readTime: '10'},
    {id: 4, title: 'COVID-19: 2 personnes localisées à Korhogo et à Duékoué acheminées en à Abidjan pour des tests médicaux',
    summary: Constants.globalSummary, image: '../../../assets/images/ambulance.jpg', datePub: '03/04/2020', readTime: '9'},
    {id: 5, title: 'Covid-19: le nombre de cas confirmés en Côte d’Ivoire s’établit à 194 dont 15 guéris',
    summary: Constants.globalSummary, image: '../../../assets/images/virus-protection.jpg', datePub: '02/04/2020', readTime: '7'},
    {id: 6, title: 'Covid 19 - Mariatou Koné: “La distribution que nous faisons n’a pas de coloration politique”',
    summary: Constants.globalSummary, image: '../../../assets/images/covid-world.jpg', datePub: '02/04/2020', readTime: '15'},
    {id: 7, title: 'Bilan du couvre-feu de la nuit du jeudi 02 avril 2020,la police nationale sauve les populations',
    summary: Constants.globalSummary, image: '../../../assets/images/covid-social-distance.jpg', datePub: '01/04/2020', readTime: '17'},
    {id: 8, title: 'L’Afrique comme laboratoire de test contre le Covid-19, Didier Drogba dit non : « Ne prenez pas les Africains comme des cobayes humains, c’est dégoûtant »',
    summary: Constants.globalSummary, image: '../../../assets/images/no-inafrica.jpg', datePub: '03/04/2020', readTime: '2'},
    {id: 9, title: 'Lutte contre la propagation du COVID-19 : les exportateurs nationaux de cacao offrent 25 millions FCFA au gouvernement ivoirien',
    summary: Constants.globalSummary, image: '../../../assets/images/funding.jpg', datePub: '03/04/2020', readTime: '5'}
  ];
}
